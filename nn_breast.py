import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn.datasets import fetch_openml
from sklearn.neural_network import MLPClassifier

dataset = datasets.load_breast_cancer()
X, y = dataset.data, dataset.target

# fig, axes = plt.subplots(figsize=(15, 10))

param = [4]
label = ["4"]
color = ["r"]

for p, l, c in zip(param, label, color):
	mlp = MLPClassifier(hidden_layer_sizes=(20, 20), activation = 'relu', max_iter=1000, alpha=0,
	                    solver='adam', verbose=True, tol=1e-4, random_state=1, batch_size = p,
	                    learning_rate_init=0.001)

	mlp.fit(X, y)
	print ("bacth size : " + l)
	print("Training set score: %f" % mlp.score(X, y))
	# print("predict: %f" % mlp.predict(X))

# 	axes.plot(mlp.loss_curve_, label=l, color = c, linestyle =  '-')

# fig.legend()
# fig.savefig('result_breast.png', bbox_inches='tight')