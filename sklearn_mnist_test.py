import matplotlib.pyplot as plt
from sklearn.datasets import fetch_openml
from sklearn.neural_network import MLPClassifier

print(__doc__)

# Load data from https://www.openml.org/d/554
X, y = fetch_openml('mnist_784', version=1, return_X_y=True)
X = X / 255.

# rescale the data, use the traditional train/test split
X_train, X_test = X[:60000], X[60000:]
y_train, y_test = y[:60000], y[60000:]

# Adam
mlp = MLPClassifier(hidden_layer_sizes=(128, 128), activation = 'relu', max_iter=100, alpha=0,
                    solver='adam', verbose=False, tol=1e-4, random_state=1, batch_size = 128,
                    learning_rate_init=0.001)

mlp.fit(X_train, y_train)
print "Training : Adam"
print("Training set score: %f" % mlp.score(X_train, y_train))
print("Test set score: %f" % mlp.score(X_test, y_test))

# Sgd
mlp2 = MLPClassifier(hidden_layer_sizes=(128, 128), activation = 'relu', max_iter=1000, alpha=0,
                    solver='sgd', verbose=False, tol=1e-4, random_state=1, momentum = 0, batch_size = 128,
                    learning_rate_init=0.001)

mlp2.fit(X_train, y_train)
print "Training : SGD"
print("Training set score: %f" % mlp2.score(X_train, y_train))
print("Test set score: %f" % mlp2.score(X_test, y_test))

fig, axes = plt.subplots(figsize=(15, 10))

axes.plot(mlp.loss_curve_, label='adam', color = 'r', linestyle =  '-')
axes.plot(mlp2.loss_curve_, label='sgd', color = 'b', linestyle =  '-')

fig.legend()
plt.show()
