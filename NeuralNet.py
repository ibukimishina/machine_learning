import numpy as np

######################
#	model setting
######################
# Number of hidden layer
num_hidden_layer = 1

# Number of neurons 
neuro_i = 3 # input layer
neuro_h = 5 # hiddned layer
neuro_o = 2 # output layer

#######################
#	learning data
#######################
data = np.loadtxt("DummyData_100_3.csv", delimiter=",")
feature,label = np.hsplit(data, [neuro_i])
datanum = len(feature)

#############################
#	activation function
#############################
def relu(z):
	return np.where(z >= 0, z, 0)

def relu_dash(z):
	return np.where(z >= 0, 1, 0)

def softmax(z):
    e = np.exp(z)
    ret = e
    length = len(e)
    for i in xrange(length):
        ret[i] = e[i] / np.sum(e, axis=1)[i]
    return ret

###############################
#	parameter initialize
###############################
# input layer -> hidden layer
w_input = np.random.rand(neuro_h, neuro_i)

# hidden layer -> output layer
w_output = np.random.rand(neuro_o, neuro_h)

# hidden layer -> hidden layer
if num_hidden_layer > 1:
	w_hidden = np.random.rand(num_hidden_layer - 1, neuro_h, neuro_h)
	grad_hidden = np.empty((num_hidden_layer - 1, neuro_h, neuro_h))

##########################
#	model initialize
##########################
v_hidden = np.empty((num_hidden_layer, datanum, neuro_h))
y_hidden = np.empty((num_hidden_layer, datanum, neuro_h))
sig_hidden = np.empty((num_hidden_layer, datanum, neuro_h))

##########################
#	parameter update
##########################
learning_rate = 0.001

for i in range(10000):

	#############################
	#	forward propagation
	#############################
	# input layer -> hidden layer
	v_hidden[0] = np.dot(feature, w_input.T)
	y_hidden[0] = relu(v_hidden[0])

	# hidden layer -> hidden layer
	if num_hidden_layer>1:
		for i in range(1, num_hidden_layer):
			v_hidden[i] = np.dot(y_hidden[i-1], w_hidden[i-1].T)
			y_hidden[i] = relu(v_hidden[i])

	# hidden layer -> ouput layer
	v_output = np.dot(y_hidden[num_hidden_layer-1], w_output.T)
	y_output = softmax(v_output)

	##########################
	#	back propagation
	##########################
	# output layer
	sig_output = y_output - label

	# output_layer -> hidden layer
	sig_hidden[num_hidden_layer-1] = relu_dash(v_hidden[num_hidden_layer-1]) * np.dot(sig_output, w_output)

	# hidden layer -> hidden layer
	if num_hidden_layer>1:
		for i in range(1, num_hidden_layer):
			sig_hidden[num_hidden_layer-(i+1)] = relu_dash(v_hidden[num_hidden_layer-(i+1)]) * np.dot(sig_hidden[num_hidden_layer-i], w_hidden[num_hidden_layer-(i+1)])

	######################
	#	calc gradient
	######################
	# input layer -> hidden layer
	grad_input = np.dot(sig_hidden[0].T, feature) / datanum
	w_input -= learning_rate * grad_input

	# hidden layer -> hidden layer
	if num_hidden_layer>1:
		for i in range(0, num_hidden_layer-1):
			grad_hidden[i] = np.dot(sig_hidden[i+1].T, y_hidden[i]) / datanum
			w_hidden[i] -= learning_rate * grad_hidden[i]

	# hidden layer -> output layer
	grad_output = np.dot(sig_output.T, y_hidden[num_hidden_layer-1]) / datanum
	w_output -= learning_rate * grad_output

################
#	predict
################
# input layer -> hidden layer
v_hidden[0] = np.dot(feature, w_input.T)
y_hidden[0] = relu(v_hidden[0])

# hidden layer -> hidden layer
if num_hidden_layer>1:
	for i in range(1, num_hidden_layer):
		v_hidden[i] = np.dot(y_hidden[i-1], w_hidden[i-1].T)
		y_hidden[i] = relu(v_hidden[i])

# hidden layer -> ouput layer
v_output = np.dot(y_hidden[num_hidden_layer-1], w_output.T)
y_output = softmax(v_output)

##############
#	check
##############
predict = np.where(y_output >= 0.5, 1, 0)
accuracy = (1.0 - np.sum(abs(predict-label), axis=0) / datanum) * 100
print accuracy[0]

np.savetxt("predict.csv", y_output, delimiter=',', fmt = "%.5f")