import numpy as np
import sys

args = sys.argv
data = np.loadtxt("test_data/input_"+ args[1] +".csv", delimiter=",")
N = len(data)
l = len(data[0])
feature, label = np.hsplit(data, [l-1])

def sigmoid(z):
    return 1.0 / (1 + np.exp(-z))

w = np.ones(l-1)
eta = 0.1

for i in xrange(int(args[2])):
    grad = np.zeros(l-1)
    for n in xrange(N):
        grad += (sigmoid(np.inner(w, feature[n])) - label[n]) * feature[n]
    w -= eta * grad / N
    print(w)